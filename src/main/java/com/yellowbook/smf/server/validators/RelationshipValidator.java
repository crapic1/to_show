package com.yellowbook.smf.server.validators;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.RelationshipConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.VersionsModel;
import com.sophicsystems.smf.server.factory.RelationshipHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.ItemVersionModel;
import com.sophicsystems.smf.server.history.model.RelationshipChangeModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.server.typedata.model.RelationshipType;

@Component("relationshipValidator")
public class RelationshipValidator extends ActionValidator<IRelationshipChangeModel> {
	@Autowired
	private RelationshipHistoryFactory relationshipHistoryFactory;
	@Value("${allow.item.name.duplication}")
	private boolean isAllowItemNameDuplication;
	private Map<String, IRelationshipChangeModel> relationshipVersionsByFromAndToAndType = new HashMap<String, IRelationshipChangeModel>();
	private List<RelationshipConflictDto> conflicts = new LinkedList<RelationshipConflictDto>();

	private ItemNameValidatorNotUniqImpl itemNameValidator;

	@Autowired
	private ApplicationContext applicationContext;

	@PostConstruct
	public void init() {
		if (isAllowItemNameDuplication) {
			itemNameValidator = (ItemNameValidatorNotUniqImpl) applicationContext.getBean("itemNameValidatorNotUniq");
			itemNameValidator.setModel(model);
		}
	}

	@Override
	public void setModel(VersionsModel model) {
		super.setModel(model);
		if (itemNameValidator != null) {
			itemNameValidator.setModel(model);
		}
	}

	@Override
	public void processChange(IChangeModel changeModel) {
		if (changeModel instanceof IRelationshipChangeModel) {
			IRelationshipChangeModel relationshipChangeModel = (IRelationshipChangeModel) changeModel;
			String key = relationshipChangeModel.getFromItemId() + "#" + relationshipChangeModel.getToItemId() + "#" + relationshipChangeModel.getRelationshipTypeId();
			relationshipVersionsByFromAndToAndType.put(key, relationshipChangeModel);
			String keyWOTO = relationshipChangeModel.getFromItemId() + "#%#" + relationshipChangeModel.getRelationshipTypeId();
			relationshipVersionsByFromAndToAndType.put(keyWOTO, relationshipChangeModel);
			if (!model.getRelationshipVersions().containsKey(relationshipChangeModel.getRelationshipId())) {
				model.getRelationshipVersions().put(relationshipChangeModel.getRelationshipId(), relationshipChangeModel.clone());
			}
		}
	}

	public void validate(RelationshipChangeModel changeModel) {
		IRelationshipChangeModel versionModel = model.getRelationshipVersions().get(changeModel.getRelationshipId());
		String key = changeModel.getFromItemId() + "#" + changeModel.getToItemId() + "#" + changeModel.getRelationshipTypeId();
		IRelationshipChangeModel versionModelByKey = relationshipVersionsByFromAndToAndType.get(key);
		String onlyOneKey = changeModel.getFromItemId() + "#%#" + changeModel.getRelationshipTypeId();

		if (versionModel != null && validateRelationshipAction(versionModel, changeModel) && !versionModel.getAction().equals(changeModel.getAction())) {
			IRelationshipChangeModel newVersionModel = versionModel.clone();
			newVersionModel.setAction(changeModel.getAction());
			model.getRelationshipVersions().put(changeModel.getRelationshipId(), newVersionModel);
		}

		IItemChangeModel fromItemVersion = model.getItemVersions().get(changeModel.getFromItemId());
		IItemChangeModel toItemVersion = model.getItemVersions().get(changeModel.getToItemId());

		if (changeModel.getAction() != ActionEnum.DELETE) {
			validateDeletedItems(changeModel, fromItemVersion, toItemVersion);
		}

		validateDuplicate(versionModelByKey, changeModel);

		validateOnlyOne(relationshipVersionsByFromAndToAndType.get(onlyOneKey), changeModel);

		if (itemNameValidator != null && model.isPrimaryRelationshipType(changeModel.getRelationshipTypeId())) {
			itemNameValidator.validate(changeModel);
		}
	}

	public Collection<RelationshipConflictDto> finishValidate() {
		return conflicts;
	}

	public void clear() {
		relationshipVersionsByFromAndToAndType.clear();
		conflicts.clear();
	}

	private boolean validateRelationshipAction(IRelationshipChangeModel versionModel, IRelationshipChangeModel changeModel) {
		if (versionModel != null) {
			String msgConflict = super.validateAction(versionModel, changeModel);
			if (msgConflict != null) {
				setToAndFromItemNames(versionModel, changeModel);
				String description = MessagesBundleHolder.getMessage("relationshipFromToMessageConflict", model.getRelationshipTypes().get(changeModel.getRelationshipTypeId()).getNameGeneric(), changeModel.getFromItemName(), changeModel.getToItemName(), msgConflict);

				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.OVERRIDE, CommitActionEnum.REVERT };
				RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, versionModel, description, actions);
				conflicts.add(conflictDto);
				return false;
			}
		}
		return true;
	}

	private void validateDeletedItems(RelationshipChangeModel changeModel, IItemChangeModel fromItemVersion, IItemChangeModel toItemVersion) {

		String msgConflict = null;
		if (ItemVersionModel.isExist(fromItemVersion) == false && ItemVersionModel.isExist(toItemVersion) == false) {
			msgConflict = " " + MessagesBundleHolder.getMessage("toAndFromItemWasDeletedInRepositoryText");
		} else if (ItemVersionModel.isExist(fromItemVersion) == false) {
			msgConflict = " " + MessagesBundleHolder.getMessage("fromItemWasDeletedInRepositoryText");
		} else if (ItemVersionModel.isExist(toItemVersion) == false) {
			msgConflict = " " + MessagesBundleHolder.getMessage("toItemWasDeletedInRepositoryText");
		}
		if (msgConflict != null) {
			final String fromItemName = fromItemVersion == null ? changeModel.getFromItemId() : fromItemVersion.getItemName();
			final String toItemName = toItemVersion == null ? changeModel.getToItemId() : toItemVersion.getItemName();
			changeModel.setFromItemName(fromItemName);
			changeModel.setToItemName(toItemName);
			String description = MessagesBundleHolder.getMessage("relationshipFromToMessageConflict", model.getRelationshipTypes().get(changeModel.getRelationshipTypeId()).getNameGeneric(), fromItemName, toItemName, msgConflict);

			CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
			RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, null, description, actions);
			conflicts.add(conflictDto);
		}
	}

	private void validateDuplicate(IRelationshipChangeModel versionModel, IRelationshipChangeModel changeModel) {
		if (versionModel != null) {
			if (!ActionEnum.DELETE.equals(changeModel.getAction()) && !ActionEnum.DELETE.equals(versionModel.getAction()) && !changeModel.getRelationshipId().equals(versionModel.getRelationshipId())) {
				setToAndFromItemNames(versionModel, changeModel);
				String description = MessagesBundleHolder.getMessage("duplicateRelationshipFromToType", changeModel.getFromItemName(), changeModel.getToItemName(), model.getRelationshipTypes().get(changeModel.getRelationshipTypeId()));

				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
				RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, versionModel, description, actions);
				conflicts.add(conflictDto);
			}
		}
	}

	private void validateOnlyOne(IRelationshipChangeModel versionModel, RelationshipChangeModel changeModel) {
		if (versionModel != null) {
			RelationshipType relationshipType = model.getRelationshipTypes().get(changeModel.getRelationshipTypeId());
			if (relationshipType.getOnlyOnePerItem() && !ActionEnum.DELETE.equals(changeModel.getAction()) && !ActionEnum.DELETE.equals(versionModel.getAction()) && !changeModel.getRelationshipId().equals(versionModel.getRelationshipId())) {
				setToAndFromItemNames(versionModel, changeModel);
				String description = MessagesBundleHolder.getMessage("onlyOneRelationshipAllowedFromToTypeConflictMessage", relationshipType.getNameGeneric(), changeModel.getFromItemName(), changeModel.getToItemName(), relationshipType.getNameGeneric());

				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
				RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, versionModel, description, actions);
				conflicts.add(conflictDto);
			}
		}
	}

	private void setToAndFromItemNames(IRelationshipChangeModel versionModel, IRelationshipChangeModel changeModel) {
		String fromItemName = model.getItemVersions().get(changeModel.getFromItemId()).getItemName();
		String toItemName = model.getItemVersions().get(changeModel.getToItemId()).getItemName();
		versionModel.setAuthor(model.getUsers().get(versionModel.getAuthorId()).getUsername());
		versionModel.setToItemName(toItemName);
		versionModel.setFromItemName(fromItemName);
		changeModel.setToItemName(toItemName);
		changeModel.setFromItemName(fromItemName);
	}

}
