package com.yellowbook.smf.server.validators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.RelationshipConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.CommitValidator;
import com.sophicsystems.smf.server.data.model.Relationship;
import com.sophicsystems.smf.server.factory.HistoryFactory;
import com.sophicsystems.smf.server.factory.RelationshipHistoryFactory;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.RelationshipChangeModel;
import com.sophicsystems.smf.server.importdata.generic.model.RelatiosnhipWrapper;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.server.typedata.dao.RelationshipTypeDao;
import com.sophicsystems.smf.server.typedata.model.RelationshipType;
import com.sophicsystems.smf.tools.MultiValueHashMap;

@Component("cycleValidator")
public class CycleValidator {
	protected static final Logger log = Logger.getLogger(CommitValidator.class);
	private static final int NO_STATE = -1;
	private static final int START_STATE = 0;
	private static final int DISCOVER_STATE = 1;
	private static final int FINISH_STATE = 2;
	private static final int FINISH_WITH_CYCLE = 3;

	@Autowired
	private RelationshipHistoryFactory relationshipHistoryFactory;
	@Autowired
	private RelationshipTypeDao relationshipTypeDao;

	@Autowired
	protected HistoryFactory historyFactory;

	//commit validator
	private Map<String, Integer> states = new HashMap<String, Integer>();
	private MultiValueHashMap<String, IRelationshipChangeModel> graph = new MultiValueHashMap<String, IRelationshipChangeModel>();
	private Map<String, IItemChangeModel> itemVersions = new HashMap<String, IItemChangeModel>();
	private Map<String, IRelationshipChangeModel> relationshipChanges = new HashMap<String, IRelationshipChangeModel>();
	private Map<Long, RelationshipType> hierachicalRelTypes;
	private LinkedList<IRelationshipChangeModel> cyclicRelationships = new LinkedList<IRelationshipChangeModel>();
	private Set<IRelationshipChangeModel> allCyclicRelationships = new HashSet<IRelationshipChangeModel>();

	//restore validator
	private Set<Relationship> path;
	private List<Set<Relationship>> pathList;
	MultiValueHashMap<String, Relationship> relationshipsByFromItem = new MultiValueHashMap<String, Relationship>();

	public Collection<RelationshipConflictDto> validate() {
		List<IRelationshipChangeModel> values = new ArrayList<IRelationshipChangeModel>(relationshipChanges.values());
		Collections.sort(values);
		for (IRelationshipChangeModel relModel : values) {
			addEdgeToGraph(relModel);
		}
		if (DFS()) {
			return getConflicts();
		}
		LinkedList<RelationshipConflictDto> linkedList = new LinkedList<RelationshipConflictDto>();
		return linkedList;
	}

	public Collection<Relationship> validate(Collection<Relationship> relationships) {
		clear();
		for (final Relationship relationship : relationships) {
			RelatiosnhipWrapper relatiosnhipWrapper = new RelatiosnhipWrapper(relationship);
			states.put(relatiosnhipWrapper.getFromItemId(), START_STATE);
			graph.put(relatiosnhipWrapper.getFromItemId(), relatiosnhipWrapper);
		}
		if (DFS()) {
			ArrayList<Relationship> list = new ArrayList<Relationship>(cyclicRelationships.size());
			for (IRelationshipChangeModel changeModel : cyclicRelationships) {
				list.add(((RelatiosnhipWrapper) changeModel).getRelationship());
			}
			clear();
			return list;
		}
		clear();
		return null;
	}

	private Collection<RelationshipConflictDto> getConflicts() {
		List<RelationshipChangeModel> cyclicRelationshipChanges = new LinkedList<RelationshipChangeModel>();
		List<RelationshipConflictDto> relationshipConflicts = new LinkedList<RelationshipConflictDto>();
		String relTypeName = hierachicalRelTypes.get(cyclicRelationships.get(0).getRelationshipTypeId()).getNameGeneric();
		String msgConflict = "";
		for (IRelationshipChangeModel relModel : cyclicRelationships) {
			if (relModel instanceof RelationshipChangeModel) {
				cyclicRelationshipChanges.add((RelationshipChangeModel) relModel);
			}
			if (!msgConflict.isEmpty()) {
				msgConflict += "->";
			}
			IItemChangeModel iItemChangeModel = itemVersions.get(relModel.getFromItemId());
			msgConflict += iItemChangeModel.getItemName() + "[" + iItemChangeModel.getSequentialId() + "]";
		}
		msgConflict = MessagesBundleHolder.getMessage("thereIsCycleForRelTypeName", msgConflict, relTypeName);

		for (RelationshipChangeModel changeModel : cyclicRelationshipChanges) {
			IItemChangeModel versionModel = itemVersions.get(changeModel.getFromItemId());
			changeModel.setFromItemName((versionModel != null) ? versionModel.getItemName()+ "[" + versionModel.getSequentialId() + "]" : MessagesBundleHolder.getMessage("nonActiveText"));
			versionModel = itemVersions.get(changeModel.getToItemId());
			changeModel.setToItemName((versionModel != null) ? versionModel.getItemName()+ "[" + versionModel.getSequentialId() + "]" : MessagesBundleHolder.getMessage("nonActiveText"));
			CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
			RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, null, msgConflict, actions);
			relationshipConflicts.add(conflictDto);
		}
		return relationshipConflicts;
	}

	public void clear() {
		states.clear();
		graph.clear();
		itemVersions = new HashMap<String, IItemChangeModel>();
		relationshipChanges = new HashMap<String, IRelationshipChangeModel>();
		cyclicRelationships.clear();
		allCyclicRelationships.clear();
		hierachicalRelTypes = null;
	}

	private void addEdgeToGraph(IRelationshipChangeModel relModel) {
		Map<Long, RelationshipType> hierachicalRelTypes = getHierarchicalRelationshipTypes();
		removeEdgeFromGraph(relModel);
		if (!hierachicalRelTypes.containsKey(relModel.getRelationshipTypeId()) || relModel.getAction() == ActionEnum.DELETE) {
			return;
		}
		states.put(relModel.getFromItemId(), START_STATE);
		graph.put(relModel.getFromItemId(), relModel);
	}

	private void removeEdgeFromGraph(IRelationshipChangeModel relModel) {
		Collection<IRelationshipChangeModel> relModels = graph.values(relModel.getFromItemId());
		if (relModels != null) {
			relModels.remove(relModel);
		}
	}

	private boolean DFS() {
		for (String itemId : graph.keySet()) {
			if (DFS(itemId)) {
				return true;
			}
		}
		return false;
	}

	private boolean DFS(String itemId) {
		states.put(itemId, DISCOVER_STATE);
		for (IRelationshipChangeModel relModel : graph.values(itemId)) {
			Integer state = states.get(relModel.getToItemId());
			state = (state == null) ? NO_STATE : state;
			if (state == START_STATE) {
				if (DFS(relModel.getToItemId())) {
					IRelationshipChangeModel firstModel = cyclicRelationships.getFirst();
					IRelationshipChangeModel lastModel = cyclicRelationships.getLast();
					if (!firstModel.getFromItemId().equals(lastModel.getToItemId())) {
						cyclicRelationships.addFirst(relModel);
					}
					return true;
				}
			} else if (state == DISCOVER_STATE) {
				cyclicRelationships.clear();
				cyclicRelationships.addFirst(relModel);
				return true;
			}
		}
		states.put(itemId, FINISH_STATE);
		return false;
	}

	private Map<Long, RelationshipType> getHierarchicalRelationshipTypes() {
		if (hierachicalRelTypes == null) {
			hierachicalRelTypes = relationshipTypeDao.findAllGenSpec();
		}
		return hierachicalRelTypes;
	}

	public void addRelationshipVersion(IRelationshipChangeModel relationshipVersions) {
		this.relationshipChanges.put(relationshipVersions.getRelationshipId(), relationshipVersions);
	}

	public void addRelationshipChange(IRelationshipChangeModel changeModel) {
		IRelationshipChangeModel pChangeModel = this.relationshipChanges.get(changeModel.getRelationshipId());
		if (pChangeModel == null || changeModel.isLocalChange() || pChangeModel.getChangeId() < changeModel.getChangeId()) {
			this.relationshipChanges.put(changeModel.getRelationshipId(), changeModel);
		}
	}

	public void setItemVersions(Map<String, IItemChangeModel> itemVersions) {
		this.itemVersions = itemVersions;
	}

	public List<Set<Relationship>> findFrom(String itemId, long itemTypeId, List<Relationship> allRaltionship) {
		relationshipsByFromItem = new MultiValueHashMap<String, Relationship>();
		for (Relationship relationship : allRaltionship) {
			if (relationship.getRelationshipTypeId() != itemTypeId) {
				continue;
			}
			relationshipsByFromItem.put(relationship.getFromItem().getId(), relationship);
		}

		path = new HashSet<Relationship>();
		pathList = new LinkedList<Set<Relationship>>();
		checkInto(relationshipsByFromItem.values(itemId));
		relationshipsByFromItem.clear();
		allRaltionship.clear();
		path.clear();
		return pathList;
	}

	private boolean checkInto(Collection<Relationship> list) {
		if (list == null) {
			return false;
		}
		for (Relationship relationship : list) {
			if (path.contains(relationship)) {
				pathList.add(new HashSet<Relationship>(path));
				continue;
			}
			path.add(relationship);
			if (checkInto(relationshipsByFromItem.values(relationship.getToItem().getId()))) {
				return true;
			}
			path.remove(relationship);
		}
		return false;
	}

	/*
	 * public static void main(String[] args) { CycleValidator cycleValidator =
	 * AbstractDataManager.getSpringContext().getBean(CycleValidator.class);
	 * Session typeSession =
	 * cycleValidator.hibernateSessionUtils.openTypeSession();
	 * cycleValidator.test();
	 * cycleValidator.hibernateSessionUtils.closeTypeSession(typeSession); }
	 */
}
