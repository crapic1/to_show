package com.yellowbook.smf.server.validators;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.ItemConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.VersionsModel;
import com.sophicsystems.smf.server.factory.ItemHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.ItemChangeModel;
import com.sophicsystems.smf.server.history.model.RelationshipChangeModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.server.typedata.model.ItemType;
import com.sophicsystems.smf.tools.MultiValueHashMap;

@Component("itemNameValidatorNotUniq")
public class ItemNameValidatorNotUniqImpl implements ItemNameValidator {

	@Autowired
	private ItemHistoryFactory itemHistoryFactory;

	private MultiValueHashMap<String, IItemChangeModel> itemsByNameAndTypeAndParent = new MultiValueHashMap<String, IItemChangeModel>();;
	private Map<String, ItemConflictDto> uniqueNameConflicts = new HashMap<String, ItemConflictDto>();
	private MultiValueHashMap<String, String> itemParentsByItemId = new MultiValueHashMap<String, String>();

	private VersionsModel model;

	@Override
	public void processItemChange(IItemChangeModel itemChangeModel) {
		Collection<String> itemParents = itemParentsByItemId.values(itemChangeModel.getItemId());
		if (itemParents == null || itemParents.isEmpty()) {
			if (ActionEnum.DELETE.equals(itemChangeModel.getAction())) {
				removeItem(itemChangeModel, null);
			} else {
				addItem(itemChangeModel, null);
			}
		} else {
			for (String itemParent : itemParents) {
				if (ActionEnum.DELETE.equals(itemChangeModel.getAction())) {
					removeItem(itemChangeModel, itemParent);
				} else {
					addItem(itemChangeModel, itemParent);
				}
			}
		}
	}

	private void addItem(IItemChangeModel itemChangeModel, String parentId) {
		String key = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId() + "#" + parentId;
		itemsByNameAndTypeAndParent.put(key, itemChangeModel);
	}

	private void removeItem(IItemChangeModel itemChangeModel, String parentId) {
		String key = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId() + "#" + parentId;
		Collection<IItemChangeModel> values = itemsByNameAndTypeAndParent.values(key);
		if (values != null) {
			for (Iterator<IItemChangeModel> iterator = values.iterator(); iterator.hasNext();) {
				IItemChangeModel iItemChangeModel = (IItemChangeModel) iterator.next();
				if (iItemChangeModel.getItemId().equals(itemChangeModel.getItemId())) {
					iterator.remove();
				}
			}
		}
		String conflictKey = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId() + "#" + parentId;
		ItemConflictDto conflictDto = uniqueNameConflicts.get(conflictKey);//remove conflicts
		if (conflictDto != null) {
			conflictDto.getItemIds().remove(itemChangeModel.getItemId());
			if (conflictDto.getItemIds().size() < 2) {
				uniqueNameConflicts.remove(conflictKey);
			}
		}

	}

	private boolean isRootItem(String itemId) {
		Collection<String> itemParents = itemParentsByItemId.values(itemId);
		return itemParents == null || itemParents.isEmpty();
	}

	@Override
	public void processRelationshipChange(IRelationshipChangeModel relationshipChangeModel) {
		if (!model.isPrimaryRelationshipType(relationshipChangeModel.getRelationshipTypeId())) {
			return;
		}
		IItemChangeModel itemChangeModel = model.getItemVersions().get(relationshipChangeModel.getToItemId());
		if (ActionEnum.DELETE.equals(relationshipChangeModel.getAction())) {
			//remove relationship
			itemParentsByItemId.remove(relationshipChangeModel.getToItemId(), relationshipChangeModel.getFromItemId());
			removeItem(itemChangeModel, relationshipChangeModel.getFromItemId());
			if (isRootItem(itemChangeModel.getItemId())) {
				addItem(itemChangeModel, null);
			}
		} else {
			itemParentsByItemId.put(relationshipChangeModel.getToItemId(), relationshipChangeModel.getFromItemId());
			removeItem(itemChangeModel, null);
			addItem(itemChangeModel, relationshipChangeModel.getFromItemId());
		}
	}

	@Override
	public void processItemUpdate(IItemChangeModel versionModel, IChangeModel changeModel) {
		removeItem(versionModel, null);
		Collection<String> itemParents = itemParentsByItemId.values(versionModel.getItemId());
		if (itemParents != null && itemParents.isEmpty() == false) {
			for (String parent : itemParents) {
				removeItem(versionModel, parent);
			}
		}
	}

	@Override
	public void clear() {
		itemsByNameAndTypeAndParent.clear();
		uniqueNameConflicts.clear();
		itemParentsByItemId.clear();
	}

	@Override
	public void validate(ItemChangeModel itemChangeModel) {
		Collection<String> itemParents = itemParentsByItemId.values(itemChangeModel.getItemId());
		if (itemParents == null || itemParents.isEmpty()) {
			validate(itemChangeModel, null);
		} else {
			for (String itemParent : itemParents) {
				validate(itemChangeModel, itemParent);
			}
		}
	}

	private void validate(IItemChangeModel itemChangeModel, String itemParent) {
		String key = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId() + "#" + itemParent;
		Collection<IItemChangeModel> existItems = itemsByNameAndTypeAndParent.values(key);
		String conflictKey = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId() + "#" + itemParent;
		if (existItems != null) {
			for (IItemChangeModel existItem : existItems) {
				if (!existItem.getItemId().equals(itemChangeModel.getItemId())) {
					ItemConflictDto conflictDto = uniqueNameConflicts.get(conflictKey);
					if (conflictDto != null) {
						conflictDto.getItemIds().add(itemChangeModel.getItemId());
					} else {
						// create conflict
						ItemType itemType = model.getItemTypes().get(itemChangeModel.getItemTypeId());
						String itemParentName;
						if (itemParent == null) {
							itemParentName = "root";
						} else {
							IItemChangeModel parentItem = model.getItemVersions().get(itemParent);
							itemParentName = parentItem.getItemName();
						}
						String itemTypeName = (itemType == null) ? MessagesBundleHolder.getMessage("nonActiveText") : itemType.getName();
						String msgConflict = MessagesBundleHolder.getMessage("duplicateItemNameInTypeAndParent", itemChangeModel.getItemName(), itemTypeName, itemParentName);
						existItem.setAuthor(model.getUsers().get(existItem.getAuthorId()).getUsername());
						CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
						conflictDto = itemHistoryFactory.createItemConflictDto(itemChangeModel, existItem, msgConflict, actions);
						uniqueNameConflicts.put(conflictKey, conflictDto);
					}
				}
			}
		}
	}

	public void validate(RelationshipChangeModel relationshipChangeModel) {
		IItemChangeModel itemChangeModel = model.getItemVersions().get(relationshipChangeModel.getToItemId());
		if (ActionEnum.DELETE.equals(relationshipChangeModel.getAction())) {
			Collection<String> values = itemParentsByItemId.values(relationshipChangeModel.getToItemId());
			if (values==null && values.size() < 2) {
				validate(itemChangeModel, null);
			}
		} else {
			validate(itemChangeModel, relationshipChangeModel.getFromItemId());
		}

	}

	@Override
	public Collection<ItemConflictDto> getConflicts() {
		return uniqueNameConflicts.values();
	}

	@Override
	public void setModel(VersionsModel model) {
		this.model = model;
	}

}
