package com.yellowbook.smf.server.validators;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.ItemConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.VersionsModel;
import com.sophicsystems.smf.server.factory.ItemHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.ItemChangeModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.server.typedata.model.ItemType;

@Component("itemNameValidatorUniq")
public class ItemNameValidatorUniqImpl implements ItemNameValidator {

	@Autowired
	private ItemHistoryFactory itemHistoryFactory;

	private Map<String, IItemChangeModel> itemVersionsByNameAndType = new HashMap<String, IItemChangeModel>();;
	private Map<Long, ItemConflictDto> uniqueNameConflicts = new HashMap<Long, ItemConflictDto>();

	private VersionsModel model;

	@Override
	public void processItemChange(IItemChangeModel itemChangeModel) {
		String key = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId();
		if (ActionEnum.DELETE.equals(itemChangeModel.getAction())) {
			itemVersionsByNameAndType.remove(key);
		} else {
			itemVersionsByNameAndType.put(key, itemChangeModel);
		}
	}

	@Override
	public void processRelationshipChange(IRelationshipChangeModel relationshipChangeModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processItemUpdate(IItemChangeModel versionModel, IChangeModel changeModel) {
		String key = versionModel.getItemName() + "#" + versionModel.getItemTypeId();
		itemVersionsByNameAndType.remove(key);
	}

	@Override
	public void clear() {
		itemVersionsByNameAndType.clear();
		uniqueNameConflicts.clear();
	}

	@Override
	public void validate(ItemChangeModel itemChangeModel) {

		String key = itemChangeModel.getItemName() + "#" + itemChangeModel.getItemTypeId();
		IItemChangeModel versionModel = itemVersionsByNameAndType.get(key);
		Long sequentialId = itemChangeModel.getSequentialId();
		ItemConflictDto conflictDto = uniqueNameConflicts.get(sequentialId);
		if (versionModel != null && !versionModel.getItemId().equals(itemChangeModel.getItemId()) && conflictDto == null) {
			// create conflict
			ItemType itemType = model.getItemTypes().get(itemChangeModel.getItemTypeId());
			String itemTypeName = (itemType == null) ? MessagesBundleHolder.getMessage("nonActiveText") : itemType.getName();
			String msgConflict = MessagesBundleHolder.getMessage("duplicateItemNameInType", itemChangeModel.getItemName(), itemTypeName);
			versionModel.setAuthor(model.getUsers().get(versionModel.getAuthorId()).getUsername());
			CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
			conflictDto = itemHistoryFactory.createItemConflictDto(itemChangeModel, versionModel, msgConflict, actions);
			uniqueNameConflicts.put(sequentialId, conflictDto);
		}
		if (versionModel == null || itemChangeModel.getAction().equals(ActionEnum.DELETE)) {
			uniqueNameConflicts.remove(sequentialId);
		}
	}

	@Override
	public Collection<ItemConflictDto> getConflicts() {
		return uniqueNameConflicts.values();
	}

	@Override
	public void setModel(VersionsModel model) {
		this.model = model;
	}
}
