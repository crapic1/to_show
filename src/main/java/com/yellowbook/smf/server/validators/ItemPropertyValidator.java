package com.yellowbook.smf.server.validators;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.ItemConflictDto;
import com.sophicsystems.smf.server.factory.ItemHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IItemPropertyChangeModel;
import com.sophicsystems.smf.server.history.model.IPropertyChangeModel;
import com.sophicsystems.smf.server.history.model.ItemPropertyChangeModel;
import com.sophicsystems.smf.server.history.model.ItemPropertyVersionModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.tools.MultiValueHashMap;

@Component("itemPropertyValidator")
public class ItemPropertyValidator extends ActionValidator<IPropertyChangeModel> {

	private Map<String, IItemPropertyChangeModel> itemPropertyVersions = new HashMap<String, IItemPropertyChangeModel>();
	private MultiValueHashMap<String, ItemConflictDto> conflicts = new MultiValueHashMap<String, ItemConflictDto>();

	@Autowired
	private ItemHistoryFactory itemHistoryFactory;

	@Override
	public void processChange(IChangeModel changeModel) {
		if (changeModel instanceof ItemPropertyVersionModel) {
			ItemPropertyVersionModel itemPropertyVersion = (ItemPropertyVersionModel) changeModel;
			String key = compositeKey(itemPropertyVersion);
			this.itemPropertyVersions.put(key, itemPropertyVersion);
		}
	}

	private String compositeKey(IItemPropertyChangeModel itemPropertyVersion) {
		return itemPropertyVersion.getItemId() + "#" + itemPropertyVersion.getPropertyTypeId() + "#" + itemPropertyVersion.getIndex();
	}

	public void validate(ItemPropertyChangeModel changeModel) {
		String key = changeModel.getItemId() + "#" + changeModel.getPropertyTypeId() + "#" + changeModel.getIndex();
		IItemPropertyChangeModel versionModel = itemPropertyVersions.get(key);
		if (versionModel != null && validateAction(versionModel, changeModel) && !versionModel.getAction().equals(changeModel.getAction())) {
			IItemPropertyChangeModel newVersionModel = versionModel.clone();
			newVersionModel.setAction(changeModel.getAction());
			itemPropertyVersions.put(key, newVersionModel);
		}

		IItemChangeModel itemVersionModel = model.getItemVersions().get(changeModel.getItemId());
		validateDeletedItems(changeModel, itemVersionModel);

	}

	public MultiValueHashMap<String, ItemConflictDto> finishValidate() {
		return conflicts;
	}

	public void clear() {
		itemPropertyVersions.clear();
		conflicts.clear();
	}

	@Override
	protected String validateAction(IPropertyChangeModel versionModel, IPropertyChangeModel changeModel) {
		ActionEnum sourceAction = changeModel.getAction();
		if (sourceAction == ActionEnum.CREATE) {
			boolean notUpdatedChange = model.isConcurrentChange(versionModel);
			switch (versionModel.getAction()) {
			case CREATE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRepositoryText");
			case UPDATE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndUpdatedInRepositoryText");
				}
			case DELETE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndDeletedInRepositoryText");
				}
			case RESTORE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRestoredInRepositoryText");
				}
			}
			return null;
		} else {
			return super.validateAction(versionModel, changeModel);
		}
	}

	private boolean validateAction(IItemPropertyChangeModel versionModel, IItemPropertyChangeModel changeModel) {
		if (versionModel != null) {
			String msgConflict = validateAction((IPropertyChangeModel) versionModel, changeModel);
			if (msgConflict != null) {
				String propertyName = model.getItemPropertyTypes().get(changeModel.getPropertyTypeId()).getName();
				String itemName = model.getItemVersions().get(changeModel.getItemId()).getItemName();
				String description = MessagesBundleHolder.getMessage("itemPropertyForItemConflictMessage", propertyName, itemName, msgConflict);

				changeModel.setPropertyTypeName(propertyName);
				versionModel.setPropertyTypeName(propertyName);
				versionModel.setAuthor(model.getUsers().get(versionModel.getAuthorId()).getUsername());
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.OVERRIDE, CommitActionEnum.REVERT };
				ItemConflictDto conflictDto = itemHistoryFactory.createItemConflictDto(changeModel, versionModel, description, actions);
				conflicts.put(changeModel.getItemId(), conflictDto);
				return false;
			}
		}
		return true;
	}

	private void validateDeletedItems(ItemPropertyChangeModel changeModel, IItemChangeModel itemVersionModel) {
		if (itemVersionModel != null) {
			ActionEnum localAction = changeModel.getAction();
			ActionEnum repositoryAction = itemVersionModel.getAction();

			String msgConflict = null;
			if (ActionEnum.CREATE.equals(localAction)) {
				if (ActionEnum.DELETE.equals(repositoryAction)) {
					msgConflict = " " + MessagesBundleHolder.getMessage("wasCreatedInDataSourceButItemWasDeletedInRepositoryText");
				}
			} else if (ActionEnum.UPDATE.equals(localAction)) {
				if (ActionEnum.DELETE.equals(repositoryAction)) {
					msgConflict = " " + MessagesBundleHolder.getMessage("wasUpdatedInDataSourceButItemWasDeletedInRepositoryText");
				}
			}
			if (msgConflict != null) {
				String propertyName = model.getItemPropertyTypes().get(changeModel.getPropertyTypeId()).getName();
				String itemName = itemVersionModel.getItemName();
				String description = MessagesBundleHolder.getMessage("itemPropertyForItemConflictMessage", propertyName, itemName, msgConflict);

				changeModel.setPropertyTypeName(propertyName);
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
				ItemConflictDto conflictDto = itemHistoryFactory.createItemConflictDto(changeModel, null, description, actions);
				conflicts.put(changeModel.getItemId(), conflictDto);
			}
		}
	}

}
