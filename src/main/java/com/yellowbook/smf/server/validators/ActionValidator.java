package com.yellowbook.smf.server.validators;

import org.springframework.beans.factory.annotation.Autowired;

import com.yellowbook.smf.client.model.ActionEnum;
import com.yellowbook.smf.server.data.manager.impl.VersionsModel;
import com.yellowbook.smf.server.factory.HistoryFactory;
import com.yellowbook.smf.server.history.model.IChangeModel;
import com.yellowbook.smf.server.messages.MessagesBundleHolder;

public abstract class ActionValidator<T extends IChangeModel> {

	@Autowired
	protected HistoryFactory historyFactory;

	VersionsModel model;

	protected String validateAction(T versionModel, T changeModel) {
		ActionEnum repositoryAction = versionModel.getAction();
		ActionEnum sourceAction = changeModel.getAction();
		switch (sourceAction) {
		case CREATE: {
			switch (repositoryAction) {
			case CREATE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRepositoryText");
			case UPDATE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndUpdatedInRepositoryText");
			case DELETE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndDeletedInRepositoryText");
			case RESTORE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRestoredInRepositoryText");
			}
			break;
		}
		case UPDATE:
			switch (repositoryAction) {
			case CREATE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasUpdatedInDatasourceAndCreatedInRepositoryText");
				} else {
					return null;
				}
			case UPDATE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasUpdatedInDatasourceAndUpdatedInRepositoryText");
				} else {
					return null;
				}
			case DELETE:
				return " " + MessagesBundleHolder.getMessage("wasUpdatedInDatasourceAndDeletedInRepositoryText");
			case RESTORE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasUpdatedInDatasourceAndRestoredInRepositoryText");
				} else {
					return null;
				}
			}
			break;
		case DELETE:
			switch (repositoryAction) {
			case CREATE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasDeletedInDatasourceAndCreatedInRepositoryText");
				} else {
					return null;
				}
			case UPDATE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasDeletedInDatasourceAndUpdatedInRepositoryText");
				} else {
					return null;
				}
			case DELETE:
				return " was deleted in datasource and repository";
			case RESTORE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasDeletedInDatasourceAndRestoredInRepositoryText");
				} else {
					return null;
				}
			}
			break;
		case RESTORE: {
			switch (repositoryAction) {
			case CREATE:
				return " " + MessagesBundleHolder.getMessage("wasRestoredInDatasourceButCreatedInRepositoryText");
			case UPDATE:
				return " " + MessagesBundleHolder.getMessage("wasRestoredInDatasourceAndUpdatedInRepositoryText");
			case DELETE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasRestoredInDatasourceAndDeletedInRepositoryText");
				} else {
					return null;
				}
			case RESTORE:
				if (model.isConcurrentChange(versionModel)) {
					return " " + MessagesBundleHolder.getMessage("wasRestoredInDatasourceAndRepositoryText");
				} else {
					return null;
				}
			}
			break;
		}
		}

		return null;
	}

	public abstract void clear();

	public void setModel(VersionsModel model) {
		this.model = model;
	}

	public abstract void processChange(IChangeModel changeModel);
}
