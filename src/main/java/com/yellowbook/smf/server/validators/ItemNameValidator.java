package com.yellowbook.smf.server.validators;

import java.util.Collection;

import com.sophicsystems.smf.client.model.commit.ItemConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.VersionsModel;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.ItemChangeModel;

public interface ItemNameValidator {

	void processItemChange(IItemChangeModel itemChangeModel);

	void processRelationshipChange(IRelationshipChangeModel relationshipChangeModel);

	void processItemUpdate(IItemChangeModel versionModel, IChangeModel changeModel);

	void clear();

	void validate(ItemChangeModel itemChangeModel);

	Collection<ItemConflictDto> getConflicts();

	void setModel(VersionsModel model);

}
