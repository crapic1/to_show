package com.yellowbook.smf.server.validators;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.RelationshipConflictDto;
import com.sophicsystems.smf.server.factory.RelationshipHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IPropertyChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipPropertyChangeModel;
import com.sophicsystems.smf.server.history.model.RelationshipPropertyChangedModel;
import com.sophicsystems.smf.server.history.model.RelationshipPropertyVersionModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.tools.MultiValueHashMap;

@Component("relationshipPropertyValidator")
public class RelationshipPropertyValidator extends ActionValidator<IPropertyChangeModel> {
	@Autowired
	private RelationshipHistoryFactory relationshipHistoryFactory;

	private Map<String, IRelationshipPropertyChangeModel> relationshipPropertyVersions = new HashMap<String, IRelationshipPropertyChangeModel>();
	private MultiValueHashMap<String, RelationshipConflictDto> conflicts = new MultiValueHashMap<String, RelationshipConflictDto>();

	@Override
	public void processChange(IChangeModel changeModel) {
		if (changeModel instanceof RelationshipPropertyVersionModel) {
			RelationshipPropertyVersionModel relationshipPropertyVersion = (RelationshipPropertyVersionModel) changeModel;
			String key = relationshipPropertyVersion.getRelationshipId() + "#" + relationshipPropertyVersion.getPropertyTypeId() + "#" + relationshipPropertyVersion.getIndex();
			this.relationshipPropertyVersions.put(key, relationshipPropertyVersion);
		}
	}

	public void validate(RelationshipPropertyChangedModel changeModel) {
		String key = changeModel.getRelationshipId() + "#" + changeModel.getPropertyTypeId() + "#" + changeModel.getIndex();
		IRelationshipPropertyChangeModel versionModel = relationshipPropertyVersions.get(key);
		if (versionModel != null && validateAction(versionModel, changeModel) && !versionModel.getAction().equals(changeModel.getAction())) {
			IRelationshipPropertyChangeModel newVersionModel = versionModel.clone();
			newVersionModel.setAction(changeModel.getAction());
			relationshipPropertyVersions.put(key, newVersionModel);
		}
		IRelationshipChangeModel itemVersionModel = model.getRelationshipVersions().get(changeModel.getRelationshipId());
		validateDeletedRelationships(changeModel, itemVersionModel);
	}

	public MultiValueHashMap<String, RelationshipConflictDto> finishValidate() {
		return conflicts;
	}

	public void clear() {
		relationshipPropertyVersions.clear();
		conflicts.clear();
	}

	@Override
	protected String validateAction(IPropertyChangeModel versionModel, IPropertyChangeModel changeModel) {
		ActionEnum sourceAction = changeModel.getAction();
		if (sourceAction == ActionEnum.CREATE) {
			boolean notUpdatedChange = model.isConcurrentChange(versionModel);
			switch (versionModel.getAction()) {
			case CREATE:
				return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRepositoryText");
			case UPDATE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndUpdatedInRepositoryText");
				}
			case DELETE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndDeletedInRepositoryText");
				}
			case RESTORE:
				if (notUpdatedChange) {
					return " " + MessagesBundleHolder.getMessage("wasCreatedInDatasourceAndRestoredInRepositoryText");
				}
			}
			return null;
		} else {
			return super.validateAction(versionModel, changeModel);
		}
	}

	private boolean validateAction(IRelationshipPropertyChangeModel versionModel, IRelationshipPropertyChangeModel changeModel) {
		if (versionModel != null) {
			String msgConflict = validateAction((IPropertyChangeModel) versionModel, changeModel);
			if (msgConflict != null) {
				String propertyName = model.getRelationshipPropertyTypes().get(changeModel.getPropertyTypeId()).getName();
				IRelationshipChangeModel relVersionModel = model.getRelationshipVersions().get(changeModel.getRelationshipId());
				String toItemName = model.getItemVersions().get(relVersionModel.getToItemId()).getItemName();
				String fromItemName = model.getItemVersions().get(relVersionModel.getFromItemId()).getItemName();
				String description = MessagesBundleHolder.getMessage("relationshipPropertyForRelationshipConflictMessage", propertyName, fromItemName, toItemName, msgConflict);

				changeModel.setPropertyTypeName(propertyName);
				versionModel.setPropertyTypeName(propertyName);
				versionModel.setAuthor(model.getUsers().get(versionModel.getAuthorId()).getUsername());
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.OVERRIDE, CommitActionEnum.REVERT };
				RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, versionModel, description, actions);
				conflicts.put(changeModel.getRelationshipId(), conflictDto);
				return false;
			}
		}
		return true;
	}

	private void validateDeletedRelationships(RelationshipPropertyChangedModel changeModel, IRelationshipChangeModel relVersionModel) {
		if (relVersionModel != null) {
			ActionEnum localAction = changeModel.getAction();
			ActionEnum repositoryAction = relVersionModel.getAction();
			String msgConflict = null;
			if (ActionEnum.CREATE.equals(localAction)) {
				if (ActionEnum.DELETE.equals(repositoryAction)) {
					msgConflict = " " + MessagesBundleHolder.getMessage("wasCreatedInDataSourceButRelationshipWasDeletedInRepository");
				}
			} else if (ActionEnum.UPDATE.equals(localAction)) {
				if (ActionEnum.DELETE.equals(repositoryAction)) {
					msgConflict = " " + MessagesBundleHolder.getMessage("wasUpdatedInDataSourceButRelationshipWasDeletedInRepository");
				}
			}
			if (msgConflict != null) {
				String propertyName = model.getRelationshipPropertyTypes().get(changeModel.getPropertyTypeId()).getName();
				String toItemName = model.getItemVersions().get(relVersionModel.getToItemId()).getItemName();
				String fromItemName = model.getItemVersions().get(relVersionModel.getFromItemId()).getItemName();
				String description = MessagesBundleHolder.getMessage("relationshipPropertyForRelationshipConflictMessage", propertyName, fromItemName, toItemName, msgConflict);

				changeModel.setPropertyTypeName(propertyName);
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
				RelationshipConflictDto conflictDto = relationshipHistoryFactory.createRelationshipConflictDto(changeModel, null, description, actions);
				conflicts.put(changeModel.getRelationshipId(), conflictDto);
			}
		}
	}

}
