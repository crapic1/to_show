package com.yellowbook.smf.server.validators;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.sophicsystems.smf.client.model.ActionEnum;
import com.sophicsystems.smf.client.model.commit.CommitActionEnum;
import com.sophicsystems.smf.client.model.commit.ItemConflictDto;
import com.sophicsystems.smf.server.data.manager.impl.VersionsModel;
import com.sophicsystems.smf.server.factory.ItemHistoryFactory;
import com.sophicsystems.smf.server.history.model.IChangeModel;
import com.sophicsystems.smf.server.history.model.IItemChangeModel;
import com.sophicsystems.smf.server.history.model.IRelationshipChangeModel;
import com.sophicsystems.smf.server.history.model.ItemChangeModel;
import com.sophicsystems.smf.server.messages.MessagesBundleHolder;
import com.sophicsystems.smf.tools.MapOfMap;

@Component("itemValidator")
public class ItemValidator extends ActionValidator<IItemChangeModel> {
	
	@Autowired
	private ItemHistoryFactory itemHistoryFactory;
	
	@Value("${allow.item.name.duplication}")
	private boolean isAllowItemNameDuplication;

	private MapOfMap<String, String, IRelationshipChangeModel> relationshipVersionsByItem = new MapOfMap<String, String, IRelationshipChangeModel>();
	private List<ItemConflictDto> conflicts = new LinkedList<ItemConflictDto>();
	private ItemNameValidator itemNameValidator;

	@Autowired
	private ApplicationContext applicationContext;

	@PostConstruct
	public void init() {
		if (isAllowItemNameDuplication) {
			itemNameValidator = (ItemNameValidator) applicationContext.getBean("itemNameValidatorNotUniq");
		} else {
			itemNameValidator = (ItemNameValidator) applicationContext.getBean("itemNameValidatorUniq");
		}
		itemNameValidator.setModel(model);
	}

	@Override
	public void setModel(VersionsModel model) {
		super.setModel(model);
		itemNameValidator.setModel(model);
	}

	@Override
	public void processChange(IChangeModel changeModel) {
		if (changeModel instanceof IItemChangeModel) {
			IItemChangeModel itemChangeModel = (IItemChangeModel) changeModel;
			itemNameValidator.processItemChange(itemChangeModel);
			if (!model.getItemVersions().containsKey(itemChangeModel.getItemId())) {
				model.getItemVersions().put(itemChangeModel.getItemId(), itemChangeModel.clone());
			}
		} else if (changeModel instanceof IRelationshipChangeModel) {
			IRelationshipChangeModel relationshipChangeModel = (IRelationshipChangeModel) changeModel;
			itemNameValidator.processRelationshipChange(relationshipChangeModel);
			relationshipVersionsByItem.put(relationshipChangeModel.getFromItemId(), relationshipChangeModel.getRelationshipId(), relationshipChangeModel);
			relationshipVersionsByItem.put(relationshipChangeModel.getToItemId(), relationshipChangeModel.getRelationshipId(), relationshipChangeModel);
		}
	}

	public void validate(IChangeModel changeModel) {
		ItemChangeModel itemChangeModel = (ItemChangeModel) changeModel;
		IItemChangeModel versionModel = model.getItemVersions().get(itemChangeModel.getItemId());
		if (versionModel != null && validateAction(versionModel, itemChangeModel) && !versionModel.getAction().equals(itemChangeModel.getAction())) {
			if (ActionEnum.UPDATE.equals(changeModel.getAction())) {
				itemNameValidator.processItemUpdate(versionModel, changeModel);
			}
			IItemChangeModel newVersionModel = versionModel.clone();
			newVersionModel.setAction(itemChangeModel.getAction());
			newVersionModel.setLastChange(itemChangeModel);
			model.getItemVersions().put(itemChangeModel.getItemId(), newVersionModel);
		}

		itemNameValidator.validate(itemChangeModel);

	}

	public Collection<ItemConflictDto> finishValidate() {
		for (IItemChangeModel versionModel : model.getItemVersions().values()) {
			if (versionModel.getAction() == ActionEnum.DELETE) {
				validateRlationships(versionModel);
			}
		}
		return getAllConflicts();
	}

	private Collection<ItemConflictDto> getAllConflicts() {
		Collection<ItemConflictDto> allConflicts = new LinkedList<ItemConflictDto>();
		allConflicts.addAll(itemNameValidator.getConflicts());
		allConflicts.addAll(conflicts);
		return allConflicts;
	}

	public void clear() {
		itemNameValidator.clear();
		conflicts.clear();
		relationshipVersionsByItem.clear();
	}

	private boolean validateAction(IItemChangeModel versionModel, ItemChangeModel changeModel) {
		if (versionModel != null) {
			String msgConflict = super.validateAction(versionModel, changeModel);
			if (msgConflict != null) {
				// create conflict
				msgConflict = MessagesBundleHolder.getMessage("ItemNameConflictMessage", changeModel.getItemName(), msgConflict);
				versionModel.setAuthor(model.getUsers().get(versionModel.getAuthorId()).getUsername());
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.OVERRIDE, CommitActionEnum.REVERT };
				ItemConflictDto conflictDto = itemHistoryFactory.createItemConflictDto(changeModel, versionModel, msgConflict, actions);
				conflicts.add(conflictDto);
				return false;
			}
		}
		return true;
	}

	private void validateRlationships(IItemChangeModel versionModel) {
		Map<String, IRelationshipChangeModel> values = relationshipVersionsByItem.values(versionModel.getItemId());
		if (values == null) {
			return;
		}
		for (IRelationshipChangeModel relationshipVersionModel : values.values()) {
			if (relationshipVersionModel != null && !relationshipVersionModel.getAction().equals(ActionEnum.DELETE)) {
				CommitActionEnum[] actions = new CommitActionEnum[] { CommitActionEnum.REVERT };
				ItemConflictDto conflictDto = itemHistoryFactory.createItemConflictDto(versionModel.getLastChange(), versionModel, MessagesBundleHolder.getMessage("relationshipForItemWasCreatedInRepositoryConflictMessage"), actions);
				conflicts.add(conflictDto);
			}
		}
	}

}